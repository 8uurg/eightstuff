package com.eightuurg.helpers;

public class DirHelper {
	public static int getYDir(int dir){
		if(dir==1)return -1;
		if(dir==0)return 1;
		return 0;
	}
	public static int getXDir(int dir){
		if(dir==4)return -1;
		if(dir==5)return 1;
		return 0;
	}
	public static int getZDir(int dir){
		if(dir==2)return -1;
		if(dir==3)return 1;
		return 0;
	}
}
