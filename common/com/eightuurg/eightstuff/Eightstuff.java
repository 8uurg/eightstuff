package com.eightuurg.eightstuff;

import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.Configuration;

import com.eightuurg.eightstuff.blocks.BlockFan;
import com.eightuurg.eightstuff.blocks.Blocks;
import com.eightuurg.eightstuff.release.ReleaseData;
import com.eightuurg.eightstuff.tileentity.TileBlockFan;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid=ReleaseData.ModId, name=ReleaseData.ModName, version=ReleaseData.Version)
@NetworkMod(clientSideRequired=true, serverSideRequired=false)

public class Eightstuff {
	@Instance
	public static Eightstuff fans;
	@SidedProxy(clientSide="com.eightuurg.eightstuff.ClientProxy", serverSide="com.eightuurg.eightstuff.CommonProxy")
	public static CommonProxy proxy;
	
	public static Logger log;
	
	public Configuration config;
	
	@PreInit
	public void PreInit(FMLPreInitializationEvent preinit){
		log = preinit.getModLog();
		config = new Configuration(preinit.getSuggestedConfigurationFile());
		
		Blocks.loadConfig(config);
		Blocks.registerBlocks();
		
		TileEntity.addMapping(TileBlockFan.class, "TileBlockFan");
		
		proxy.renderTickRegister();
	}
	
	@Init
	public void Init(FMLInitializationEvent init){
		Blocks.registerRecipes();
	}
	
	public static String getRes(String flb){
		return ReleaseData.ModName + ":" + flb;
	}
}
