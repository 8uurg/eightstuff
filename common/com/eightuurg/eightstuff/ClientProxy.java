package com.eightuurg.eightstuff;

import com.eightuurg.eightstuff.render.RenderTick;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

public class ClientProxy extends CommonProxy {
	@Override
	public void registerBlockRenderers(){
	}
	
	@Override
	public void renderTickRegister(){
		TickRegistry.registerTickHandler(new RenderTick(), Side.CLIENT);
	}
}
