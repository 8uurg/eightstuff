package com.eightuurg.eightstuff.material;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

public class MaterialSlime extends Material{

	public MaterialSlime() {
		super(MapColor.foliageColor);
	}

}
