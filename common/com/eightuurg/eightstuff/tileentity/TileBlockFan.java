package com.eightuurg.eightstuff.tileentity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.PlayerCapabilities;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

import com.eightuurg.eightstuff.Eightstuff;
import com.eightuurg.eightstuff.blocks.Blocks;
import com.eightuurg.helpers.DirHelper;

public class TileBlockFan extends TileEntity {
	
	//public ArrayList<Entity> pushedInTick = new ArrayList<Entity>();
	private int pwrx = 1; 
	private boolean alwaysmax = false;
	private int prid = 0;
	
	public TileBlockFan(Block parent){
		prid = parent.blockID;
	}
	public TileBlockFan(int pow, Block parent){
		this(parent);
		if(pow!=0)pwrx = pow;
	}
	
	@Override
	public void updateEntity(){
		int height = 7;
		int boostfactor = 0;
		World wrld = this.getWorldObj(); int x= this.xCoord; int y = this.yCoord; int z = this.zCoord;
		int basepower = 1-wrld.getBlockPowerInput(x, y, z)/15;
		int direction = wrld.getBlockMetadata(x, y, z);
		int xd = DirHelper.getXDir(direction);int yd = DirHelper.getYDir(direction);int zd = DirHelper.getZDir(direction);
		//int add = xd + zd + yd;
		while(wrld.getBlockId(x-boostfactor*xd, y-boostfactor*yd, z-boostfactor*zd)==prid)boostfactor++;
		height = height * boostfactor;
		for(int i=0; i<height; i++){
			if(wrld.isBlockNormalCube(x+(i+1)*xd, y+(i+1)*yd, z+(i+1)*zd)){height=i;break;}
		} 
		if(height==0||boostfactor==0)return;
		int ma1 = x+Math.abs(xd); int ma2 = y+Math.abs(yd); int ma3 = z+Math.abs(zd); int mb1 = x+height*xd+(xd==0?1:0); int mb2 = y+height*yd+(yd==0?1:0); int mb3 = z+height*zd+(zd==0?1:0);
		List<Entity> a = wrld.getEntitiesWithinAABB(Entity.class, AxisAlignedBB.getBoundingBox(lowest(ma1,mb1), lowest(ma2,mb2), lowest(ma3,mb3), highest(mb1,ma1), highest(mb2,ma2), highest(mb3,ma3)));
		//Eightstuff.log.info("A1" + ma1 + "A2" + ma2 + "A3" + ma3 + "B1" + mb1 + "B2" + mb2 + "B3" + mb3);
		double velproduct = 0.30;
		double minforce = 20;
		for(Entity e:a){
			double ex = e.posX; double ey = e.posY; double ez = e.posZ;
			double diffx = Math.abs(x-ex);
			double diffy = Math.abs(y-ey);
			double diffz = Math.abs(z-ez);
			if(alwaysmax){diffx = 0; diffy = 0; diffz = 0;}
			double velx = velproduct*xd*basepower*pwrx*((height-diffx+minforce)/(height+minforce));
			double vely = velproduct*yd*basepower*pwrx*((height-diffy+minforce)/(height+minforce));
			double velz = velproduct*zd*basepower*pwrx*((height-diffz+minforce)/(height+minforce));
			if(height-diffx<=0.0&&xd!=0&&pwrx>0)velx=0;
			if(height-diffy<=0.0&&yd!=0&&pwrx>0)vely=0;
			if(height-diffz<=0.0&&zd!=0&&pwrx>0)velz=0;
			if(xd==0||farFromZero(e.motionX,velx)==e.motionX)velx=e.motionX;
			if(yd==0||farFromZero(e.motionY,vely+1.5)==e.motionY)vely=e.motionY;
			if(zd==0||farFromZero(e.motionZ,velz)==e.motionZ)velz=e.motionZ;
			/*velx = velx + neg(e.motionX,velx);
			vely = vely + neg(e.motionY,vely);
			velz = velz + neg(e.motionZ,velz);*/
			//Eightstuff.log.info("xd: " + zd);
				if(e instanceof EntityPlayer){
					EntityPlayer plx = (EntityPlayer)e;
					PlayerCapabilities plxc = plx.capabilities;
					if(plxc.isCreativeMode&&plxc.isFlying)continue;
					if(plx.isSneaking())continue;
				}
				e.setVelocity(velx, vely, velz);
				if(vely>0)e.fallDistance = 0;
		}
	}
	
	private double neg(double a, double b){
		if(a<0&&b<0)return 0;
		if(a>0&&b>0)return 0;
		return a;
	}
	
	private double farFromZero(double a, double b){
		if(Math.abs(a)>Math.abs(b))return a;
		return b;
	}
	
	private double ifzeroother(double a, double b){
		if(a==0)return b;
		return a;
	}
	
	private double lowest(double a, double b){
		if(a>b)return b;
		return a;
	}
	private double highest(double a, double b){
		if(a<b)return b;
		return a;
	}
	private double between(double a, double bw, double b){
		return highest(lowest(bw,b),a);
	}
	
}
