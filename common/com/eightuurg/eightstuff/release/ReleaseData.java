package com.eightuurg.eightstuff.release;

public class ReleaseData {
	/***
	 * Format:	prefix the version with:
	 *				a for alpha
	 *				b for beta
	 *				rc for release canidate
	 *				r for release
	 *
	 *			Version number is <main>.<sub>.[patch]-(MC<version>|Universal)
	 *			When version can be used from 1.5 and beyond, use universal!
	 * TODO		Please update this before release!
	 */
	public final static String Version = "a1.0-MC1.5.1";	
	//Do not change the following lines
	public final static String ModName = "Eightstuff";
	public final static String ModId = "Eightstuff";
}
