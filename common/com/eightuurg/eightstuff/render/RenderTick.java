package com.eightuurg.eightstuff.render;

import java.util.EnumSet;

import org.lwjgl.opengl.GL11;

import com.eightuurg.eightstuff.material.Materials;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class RenderTick implements ITickHandler {

	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
		GuiIngame gui = Minecraft.getMinecraft().ingameGUI;
		Minecraft mc = Minecraft.getMinecraft();
		if(mc.thePlayer==null)return;
		if(gui==null)return;
		if(mc.thePlayer.isInsideOfMaterial(Materials.slime)&&mc.gameSettings.thirdPersonView == 0){
			renderSlimeOverlay();
		}
	}
	
	private void renderSlimeOverlay(){
		 Minecraft mc = Minecraft.getMinecraft();
		 GL11.glDisable(GL11.GL_DEPTH_TEST);
	     GL11.glDepthMask(false);
	     GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	     GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	     GL11.glDisable(GL11.GL_ALPHA_TEST);
	     mc.renderEngine.bindTexture("/mods/Eightstuff/textures/blur/slimeBlur.png");
	     Tessellator tessellator = Tessellator.instance;
	     tessellator.startDrawingQuads();
	     ScaledResolution res = new ScaledResolution(mc.gameSettings, mc.displayWidth, mc.displayHeight);
	     double width = res.getScaledWidth_double();
	     double height = res.getScaledHeight_double();
	     tessellator.addVertexWithUV(0.0D, height, -90.0D, 0.0D, 1.0D);
	     tessellator.addVertexWithUV(width, height, -90.0D, 1.0D, 1.0D);
	     tessellator.addVertexWithUV(width, 0.0D, -90.0D, 1.0D, 0.0D);
	     tessellator.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
	     tessellator.draw();
	     GL11.glDepthMask(true);
	     GL11.glEnable(GL11.GL_DEPTH_TEST);
	     GL11.glEnable(GL11.GL_ALPHA_TEST);
	     GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.RENDER);
	}

	@Override
	public String getLabel() {
		return null;
	}

}
