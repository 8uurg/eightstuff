package com.eightuurg.eightstuff.blocks;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class BlockSlimeCushion extends BlockSlime{

	public BlockSlimeCushion(int par1) {
		super(par1);
		setUnlocalizedName("Slime Cushion");
	}
	
	@Override
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
    {
		this.setBlockBounds(0F, 0F, 0F, 1F, 0.1F, 1F);
		super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity,true);
		this.setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
    }
}
