package com.eightuurg.eightstuff.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;

public class Blocks {
	public static BlockBasicFan basicFan;
	public static BlockFan suckFan;
	public static BlockSlime blockSlime;
	public static BlockSlimeCushion slimeCushion;
	
	public static int basicFanID = 501;
	public static int suckFanID = 502;
	public static int slimeBlockID = 503;
	public static int slimeCushionID = 504;
	
	public static void loadConfig(Configuration config){
		basicFanID = config.getBlock("basicFan", basicFanID).getInt();
		suckFanID = config.getBlock("suckFan", suckFanID).getInt();
		slimeBlockID = config.getBlock("slimeBlock", slimeBlockID).getInt();
		slimeCushionID = config.getBlock("slimeCushionBlock", slimeBlockID).getInt();
	}
	
	public static void registerBlocks(){
		basicFan = new BlockBasicFan(basicFanID);
		GameRegistry.registerBlock(basicFan,"BasicFan");
		LanguageRegistry.addName(basicFan, "Basic Fan");
		
		suckFan = new BlockSuckFan(suckFanID);
		GameRegistry.registerBlock(suckFan,"SuckFan");
		LanguageRegistry.addName(suckFan, "Suction Fan");
		
		blockSlime = new BlockSlime(slimeBlockID);
		GameRegistry.registerBlock(blockSlime,"SlimeBlock");
		LanguageRegistry.addName(blockSlime, "Slime Block");
		
		slimeCushion = new BlockSlimeCushion(slimeCushionID);
		GameRegistry.registerBlock(slimeCushion,"SlimeCushionBlock");
		LanguageRegistry.addName(slimeCushion, "Slime Cushion");
	}
	
	public static void registerRecipes(){
		GameRegistry.addShapedRecipe(new ItemStack(basicFan,4), "III", "CPC", Character.valueOf('I'), Item.ingotIron, Character.valueOf('C'), Block.cobblestone, Character.valueOf('P'), Block.pistonBase);
		GameRegistry.addShapedRecipe(new ItemStack(suckFan,4), "III", "CPC", Character.valueOf('I'), Item.ingotIron, Character.valueOf('C'), Block.cobblestone, Character.valueOf('P'), Block.pistonStickyBase);
		GameRegistry.addShapedRecipe(new ItemStack(blockSlime), "SS","SS", Character.valueOf('S'), Item.slimeBall);
		GameRegistry.addShapelessRecipe(new ItemStack(slimeCushion), blockSlime, Block.pressurePlatePlanks);
	}
}
