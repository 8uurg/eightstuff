package com.eightuurg.eightstuff.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.eightuurg.eightstuff.Eightstuff;
import com.eightuurg.eightstuff.tileentity.TileBlockFan;
import com.eightuurg.helpers.DirHelper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockFan extends BlockContainer {
	
	private Icon itop;
	private Icon itopa;
	private Icon iside;
	private Icon ibottom;
	private int pwrx = 1;
	
	public BlockFan(int id, int pow) {
		super(id, Material.rock);
		setTickRandomly(true);
		setCreativeTab(CreativeTabs.tabRedstone);
		if(pow!=0)pwrx = pow; 
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileBlockFan(pwrx, this);
	}

	@Override
	public Icon getBlockTextureFromSideAndMetadata(int side, int met){
		int rmeta = met;
		if(rmeta==1)met=0;
		if(rmeta==0)met=1;
		if(side==met)return itop;
		if(side==getOtherDir(met))return ibottom;
		if(side==1||side==0)return ibottom;
		return iside;
	}
	
	@Override
	public Icon getBlockTexture(IBlockAccess blx, int x, int y, int z, int s){
		int met = blx.getBlockMetadata(x, y, z);
		Icon a = getBlockTextureFromSideAndMetadata(s,met);
		int rmeta = met;
		if(rmeta==1)met=0;
		if(rmeta==0)met=1;
		if(met==s&&isBlockPowered(blx,x,y,z)!=15)return itopa;
		return a;
	}
	
	@Override
	public void registerIcons(IconRegister ir){
		itop = ir.registerIcon(Eightstuff.getRes("fan_top"));
		itopa = ir.registerIcon(Eightstuff.getRes("fan_top-anim"));
		iside = ir.registerIcon(Eightstuff.getRes("fan_side"));
		ibottom = ir.registerIcon(Eightstuff.getRes("fan_bottom"));
    }
	
	/*@Override
	public void updateTick(World wrld, int x, int y, int z, Random rnd){
		
	}*/
	
	@Override
	public int tickRate(World par1World){
        return 3;
    }
	
	@Override
	public void onBlockPlacedBy(World wrld, int x, int y, int z, EntityLiving plr, ItemStack stck)
	{
		int dir;
		double diffX = Math.abs(x-plr.posX);
		double diffZ = Math.abs(z-plr.posZ);
		if(diffX+diffZ<2){//Standing really close -> up or down: 1 or 0
			if(plr.posY>y)dir=0;else dir=1;
		}else{
			if(diffX>diffZ){//diffX higher than diffZ -> x-axis pos or neg: 5 or 4
				if(plr.posX>x)dir=5; else dir=4;
			}else{//z-axis pos or neg: 3 or 2
				if(plr.posZ>z)dir=3; else dir=2;
			}
		}
		
		wrld.setBlockMetadataWithNotify(x, y, z, dir, 1);
	}
	
	private int getOtherDir(int dir){
		if(dir%2==1)return dir-1;else return dir+1;
	}
	private int isBlockPowered(IBlockAccess blx ,int x, int y, int z){
		int a = 0;
		a=(int)highest(blx.isBlockProvidingPowerTo(x-1, y, z, 4),a);
		a=(int)highest(blx.isBlockProvidingPowerTo(x+1, y, z, 5),a);
		a=(int)highest(blx.isBlockProvidingPowerTo(x, y-1, z, 0),a);
		a=(int)highest(blx.isBlockProvidingPowerTo(x, y+1, z, 1),a);
		a=(int)highest(blx.isBlockProvidingPowerTo(x, y, z-1, 2),a);
		a=(int)highest(blx.isBlockProvidingPowerTo(x, y, z+1, 3),a);
		return a;
	}
	private double highest(double a, double b){
		if(a<b)return b;
		return a;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World wrld, int x, int y, int z, Random rnd) {
		if(wrld.getBlockPowerInput(x, y, z)!=15){
			int meta = wrld.getBlockMetadata(x, y, z);
			int xd = DirHelper.getXDir(meta); int yd = DirHelper.getYDir(meta); int zd = DirHelper.getZDir(meta);
			wrld.spawnParticle("smoke", x+0.5+0.5*xd, y+0.5+0.5*yd, z+0.5+0.5*zd, xd/5, yd/5, zd/5);
		}
	}
}
