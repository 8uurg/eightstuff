package com.eightuurg.eightstuff.blocks;

import java.util.List;

import com.eightuurg.eightstuff.Eightstuff;
import com.eightuurg.eightstuff.material.Materials;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockSlime extends Block {
	
	private Icon noTexture = null;
	
	public static int renderID = 0;
	
	public BlockSlime(int par1) {
		super(par1, Materials.slime);
		setUnlocalizedName("Slime Block");
		this.setCreativeTab(CreativeTabs.tabRedstone);
		// TODO Auto-generated constructor stub
	}
	
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity, boolean a)
    {
		super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
    }
	
	@Override
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
    {
		this.setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
    }
	
	@Override
	public boolean shouldSideBeRendered(IBlockAccess b, int x, int y, int z, int s)
    {
		if(b.isBlockNormalCube(x,y,z)&&b.isBlockOpaqueCube(x, y, z))return false;
		if(b.getBlockId(x, y, z)==blockID)return false;
        return true;
    }
	
	@Override
	public Icon getBlockTexture(IBlockAccess b, int x, int y, int z, int s)
    {
       /* if(s==0&&!b.isAirBlock(x,y-1,z))return noTexture;
        if(s==1&&!b.isAirBlock(x,y+1,z))return noTexture;
        if(s==2&&!b.isAirBlock(x,y,z-1))return noTexture;
        if(s==3&&!b.isAirBlock(x,y,z+1))return noTexture;
        if(s==4&&!b.isAirBlock(x-1,y,z))return noTexture;
        if(s==5&&!b.isAirBlock(x+1,y,z))return noTexture;*/
		return blockIcon;
    }
	
	@Override
	public void onFallenUpon(World w, int x, int y, int z, Entity e, float par) {
		e.fallDistance = 0;
	}
	
	@Override
	public void onEntityCollidedWithBlock(World w, int x, int y, int z, Entity e) {
		if(e.motionX>0.01)e.motionX=0.01;
		if(e.motionZ>0.01)e.motionZ=0.01;
		if(e.motionX<-0.01)e.motionX=0.01;
		if(e.motionZ<-0.01)e.motionZ=0.01;
		if(e.motionY<-0.01)e.motionY=-0.01;
		if(e.motionY<0)e.motionY = e.motionY/32;
	}
	@SideOnly(Side.CLIENT)
	@Override
	public int getRenderBlockPass()
    {
        return 1;
    }
	
	@Override
	public boolean renderAsNormalBlock(){
        return false;
    }
	
	@Override
	public boolean isOpaqueCube(){
        return false;
    }
	
	@Override
	public void registerIcons(IconRegister iconr){
        blockIcon = iconr.registerIcon(Eightstuff.getRes("blockSlime"));
        //noTexture = iconr.registerIcon(Eightstuff.getRes("notexture"));
    }
}
