package com.eightuurg.eightstuff.blocks;

public class BlockBasicFan extends BlockFan{
	
	public BlockBasicFan(int id) {
		super(id, 1);
		setUnlocalizedName("BasicFan");
	}
	
}
